/*
 * sampling.h
 *
 *  Created on: Nov 28, 2020
 *      Author: Patroklos
 */

#ifndef INC_SAMPLING_H_
#define INC_SAMPLING_H_

/* Includes */

/* Definitions */

/* Interface */
extern void SAMPLING_task(void);

#endif /* INC_SAMPLING_H_ */
