/*
 * ads1256.h
 *
 *  Created on: Nov 28, 2020
 *      Author: Patroklos
 */

#ifndef INC_ADS1256_H_
#define INC_ADS1256_H_

/* Includes */
#include "cmsis_os.h"

/* ADS1256 Registers (see p30 for full Register Map) */
typedef enum {
	ADSREG_STATUS  = 0x00, //Status Control Register 0
	ADSREG_MUX     = 0x01, //Multiplexer Control Register 0
	ADSREG_ADCON   = 0x02, //A/D Control Register 0
	ADSREG_DRATE   = 0x03, //A/D Data Rate Control Register 0
} ADS_reg_t;

/* Helper definitions for setting those registers */
#define STATUS_ORDER_MSB 	0b00000000 // Most significant Bit first (default)
#define STATUS_ORDER_LSB 	0b00001000//Least significant Bit first
#define STATUS_ACAL_OFF 	0b00000000 // Auto Calibration Disabled (default)
#define STATUS_ACAL_ON  	0b00000100 // Auto Calibration Enabled
#define STATUS_BUFEN_OFF 	0b00000000 //Buffer Disabled (default)
#define STATUS_BUFEN_ON  	0b00000010 //BUffer Enabled

#define ADCON_PGA_1 0 //(default)
#define ADCON_PGA_2 1
#define ADCON_PGA_4 2
#define ADCON_PGA_8 3
#define ADCON_PGA_16 4
#define ADCON_PGA_32 5
#define ADCON_PGA_64 7

#define DRATE_30000 0b11110000 //30.000 SPS (default)
#define DRATE_15000 0b11100000 //15.000 SPS
#define DRATE_7500  0b11010000 //7.500 SPS
#define DRATE_3750  0b11000000 //3.750 SPS
#define DRATE_2000  0b10110000 //2.000 SPS
#define DRATE_1000  0b10100001 //1.000 SPS
#define DRATE_500   0b10010010 //500 SPS
#define DRATE_100   0b10000010 //100 SPS
#define DRATE_60    0b01110010 //60 SPS
#define DRATE_50    0b01100011 //50 SPS
#define DRATE_30    0b01010011 //30 SPS
#define DRATE_25    0b01000011 //25 SPS
#define DRATE_15    0b00110011 //15 SPS
#define DRATE_10    0b00100011 //10 SPS
#define DRATE_5     0b00010011 //5 SPS
#define DRRATE_2_5  0b00000011 //2,5 SPS

/* SPI COMMAND DEFINITIONS (p34) */
typedef enum {
	CMD_WAKEUP  = 0x00,  //Exit Sleep Mode
	CMD_SYNC    = 0xFC,  //Synchronize the A/D Conversion
	CMD_RESET   = 0xFE,  //Reset To Power UP values
	CMD_RDATA   = 0x01,  //Read data once
	CMD_RREG    = 0x10,  //Read From Register
	CMD_WREG    = 0x50,  //Write To Register
	CMD_SELFCAL = 0xF0,  //Self Offset and Gain Calibration
} ADS_cmd_t;

/* Time constants */
#define	T6	8 //t6 delay in us for Fosc=7.68Mhz
#define	T11	4 //t11 delay in us for Fosc=7.68Mhz

/* Interface */
extern void ADS_cmd(ADS_cmd_t command);
extern void ADS_writeReg(ADS_reg_t reg, uint8_t value);
extern uint8_t ADS_readReg(ADS_reg_t reg);
extern int32_t ADS_readDat(void);
extern void ADS_init(void);
extern void ADS_waitRDY(void);
extern void ADS_CS1(uint8_t state);
extern void ADS_CS2(uint8_t state);

#endif /* INC_ADS1256_H_ */
