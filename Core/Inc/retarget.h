#ifndef _RETARGET_H__
#define _RETARGET_H__

#include "stm32wbxx_hal.h"
#include <sys/stat.h>

void RetargetInit(UART_HandleTypeDef *huart);
int _write(int fd, char* ptr, int len);

#endif //#ifndef _RETARGET_H__
