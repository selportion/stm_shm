/* This file includes all relevant functions about communication to the ADCs and sampling */

/* Includes */
#include "sampling.h"
#include "main.h"
#include "cmsis_os.h"
#include "ads1256.h"
#include <stdio.h>

/* Definitions and constants */
#define MEAS_BUFFLEN		1000
#define MEAS_CHANNELSNUM	8
#define MEAS_SAMPLING_T_US	3000

typedef struct {
	float seconds;
	int32_t value;
} measurement_t;

/* Private Function declarations */
static float get_RTC_s(void);
static void SAMPLING_printbuffer(uint8_t channel_id);

/* Private variables */
RTC_HandleTypeDef* hrtc_ptr;
UART_HandleTypeDef* huart1_ptr;
TIM_HandleTypeDef* htim17_ptr;

measurement_t s_meas1[MEAS_CHANNELSNUM][MEAS_BUFFLEN];
measurement_t s_meas2[MEAS_CHANNELSNUM][MEAS_BUFFLEN];
uint8_t s_muxchannels[MEAS_CHANNELSNUM] = { 0x09, 0x19, 0x29 };

extern osSemaphoreId_t isSamplingTimeElapsedHandle;
extern osSemaphoreId_t isAdsRdyHandle;

/* Function definitions */
static float get_RTC_s(void)
{
	//TODO: Check LSI vs LSE clock source for accuracy and consider not using HAL
	RTC_TimeTypeDef currTime = {0};
	RTC_DateTypeDef currDate = {0};
	HAL_RTC_GetTime(hrtc_ptr, &currTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(hrtc_ptr, &currDate, RTC_FORMAT_BIN);
	float seconds = currTime.Seconds + (0.999 - (currTime.SubSeconds * 0.999 / currTime.SecondFraction));
	return seconds;
}

static void SAMPLING_printbuffer(uint8_t channel_id)
{
	printf("---Channel %d ---\n\r", channel_id);
	for (uint16_t i = 0; i < MEAS_BUFFLEN; i++) {
		//printf("%f, %ld \n\r", s_meas1[channel_id][i].seconds, s_meas1[channel_id][i].value);
		printf("%ld \n\r", s_meas1[channel_id][i].value);
	}
}

extern void SAMPLING_task(void)
{
	hrtc_ptr = MAIN_get_hrtc();
	huart1_ptr = MAIN_get_huart1();
	htim17_ptr = MAIN_get_htim17();

	//Update Tim17 period to sampling T target
	__HAL_TIM_SET_AUTORELOAD(htim17_ptr,MEAS_SAMPLING_T_US*32/2);
	HAL_TIM_Base_Start_IT(htim17_ptr);

	printf("--- Initializing ADS ---\n\r");
	ADS_init();
	printf("--- Done ---\n\r");

	static uint16_t meas_pos = 0;

	/* Discard first read */
	/* Wait to be notified that the ads ready pin is asserted  */
	//osSemaphoreAcquire(isAdsRdyHandle, osWaitForever);
	ADS_waitRDY();
	/* Wait to be notified that the time to sample has elapsed.  */
	osSemaphoreAcquire(isSamplingTimeElapsedHandle, osWaitForever);

	ADS_CS1(0);
	ADS_CS2(0);
	ADS_writeReg(ADSREG_MUX, s_muxchannels[0]);
	ADS_cmd(CMD_SYNC);
	ADS_cmd(CMD_WAKEUP);
	ADS_CS2(1);
	ADS_readDat();
	ADS_CS1(1);
	ADS_CS2(0);
	ADS_readDat();
	ADS_CS2(1);

  /* Infinite loop */
  for(;;)
  {
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);

	  /* Wait to be notified that the time to sample has elapsed.  */
	  osSemaphoreAcquire(isSamplingTimeElapsedHandle, osWaitForever);

	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);

	  for (uint8_t channel_id = 0; channel_id < MEAS_CHANNELSNUM; channel_id++) {

		  ADS_waitRDY();

		  ADS_CS1(0);
		  ADS_CS2(0);
		  ADS_writeReg(ADSREG_MUX, s_muxchannels[channel_id]);
		  ADS_cmd(CMD_SYNC);
		  ADS_cmd(CMD_WAKEUP);

		  /* Get the previous channel's value */
		  ADS_CS2(1);
		  if (channel_id >= 1) {
			  s_meas1[channel_id-1][meas_pos].value = ADS_readDat();
		  }
		  ADS_CS1(1);
		  ADS_CS2(0);
		  if (channel_id >= 1) {
			  s_meas2[channel_id-1][meas_pos].value = ADS_readDat();
		  }
		  ADS_CS2(1);

	  }

	  /* Get the last channel's value */
	  ADS_waitRDY();
	  ADS_CS1(0);
	  s_meas1[MEAS_CHANNELSNUM-1][meas_pos].value = ADS_readDat();
	  ADS_CS1(1);
	  ADS_CS2(0);
	  s_meas2[MEAS_CHANNELSNUM-1][meas_pos].value = ADS_readDat();
	  ADS_CS2(1);

	  printf("%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,%ld,\n\r",
			  s_meas1[0][meas_pos].value, s_meas1[1][meas_pos].value, s_meas1[2][meas_pos].value, s_meas1[3][meas_pos].value,
			  s_meas1[4][meas_pos].value, s_meas1[5][meas_pos].value, s_meas1[6][meas_pos].value, s_meas1[7][meas_pos].value,
			  s_meas2[0][meas_pos].value, s_meas2[1][meas_pos].value, s_meas2[2][meas_pos].value, s_meas2[3][meas_pos].value,
			  s_meas2[4][meas_pos].value, s_meas2[5][meas_pos].value, s_meas2[6][meas_pos].value, s_meas2[7][meas_pos].value);
	  if (++meas_pos>=MEAS_BUFFLEN) {
		  meas_pos = 0;
		  //SAMPLING_printbuffer(0);
	  }

  }

}
