/*
 * ads1256.c
 *
 *  Created on: Dec 3, 2020
 *      Author: Patroklos
 */

/* Includes */
#include "cmsis_os.h"
#include "ads1256.h"
#include "main.h"

/* Definitions and constants */
#define USER_STATUS_ORDER	STATUS_ORDER_MSB
#define USER_STATUS_ACAL	STATUS_ACAL_OFF
#define USER_STATUS_BUFFEN	STATUS_BUFEN_ON
#define USER_ADCON_PGA		ADCON_PGA_2
#define USER_DRATE			DRATE_30000

/* Private Function declarations */
static void delay_us (uint32_t us);

/* Private variables */
TIM_HandleTypeDef* htim2_ptr;
SPI_HandleTypeDef* hspi1_ptr;

/* Function definitions */
static void delay_us (uint32_t us)
{
	__HAL_TIM_SET_COUNTER(htim2_ptr,0);  // set the counter value a 0
	uint32_t tim_val = __HAL_TIM_GET_COUNTER(htim2_ptr);
	while ( tim_val < 32*us){
		tim_val = __HAL_TIM_GET_COUNTER(htim2_ptr);  // wait for the counter to reach the us input in the parameter
	}
}

extern void ADS_cmd(ADS_cmd_t command)
{
	uint8_t single_byte_cmd = (uint8_t) command;
	HAL_SPI_Transmit(hspi1_ptr,&single_byte_cmd,1,0xffff);
	//delay_us(T11);
}

extern void ADS_writeReg(ADS_reg_t reg, uint8_t value)
{
	uint8_t bytes_to_send[3] = { CMD_WREG|(uint8_t)reg, 0, value };
	HAL_SPI_Transmit(hspi1_ptr,bytes_to_send,3,0xffff);	//TODO: Replace with a function of non-blocking calls
}

extern uint8_t ADS_readReg(ADS_reg_t reg)
{
	uint8_t result = 0;
	uint8_t bytes_to_send[2] = { CMD_RREG|(uint8_t)reg, 0 };
	HAL_SPI_Transmit(hspi1_ptr,bytes_to_send,2,0xffff);	//TODO: Replace with a function of non-blocking calls
	delay_us(T6);
	HAL_SPI_Receive(hspi1_ptr,&result,1,0xffff);
	return result;
}

extern int32_t ADS_readDat(void)
{
	int32_t result = 0;
	uint8_t readback[3] = {0};
	uint8_t bytes_to_send = CMD_RDATA;
	HAL_SPI_Transmit(hspi1_ptr,&bytes_to_send,1,0xffff);	//TODO: Replace with a function of non-blocking calls
	//delay_us(T6);
	HAL_SPI_Receive(hspi1_ptr,readback,3,0xffff);
	result = (readback[0]<<16) | (readback[1]<<8) | (readback[2]<<0);
	return result;
}

extern void ADS_init(void)
{
	htim2_ptr = MAIN_get_htim2();
	hspi1_ptr = MAIN_get_hspi1();

	HAL_TIM_Base_Start(htim2_ptr);

	ADS_waitRDY();
	ADS_CS1(0);
	ADS_CS2(0);
	ADS_cmd(CMD_RESET);
	ADS_waitRDY();
	delay_us(3000);
	ADS_writeReg(ADSREG_STATUS, USER_STATUS_ORDER | USER_STATUS_ACAL | USER_STATUS_BUFFEN);
	ADS_waitRDY();
	ADS_writeReg(ADSREG_ADCON, USER_ADCON_PGA);
	ADS_waitRDY();
	ADS_writeReg(ADSREG_MUX, 0x09);
	ADS_waitRDY();
	ADS_writeReg(ADSREG_DRATE, USER_DRATE);
	ADS_waitRDY();
	ADS_cmd(CMD_SELFCAL);
	ADS_waitRDY();
	delay_us(1000);
	ADS_CS2(1); //Keep ADS1 selected only first
	uint8_t readback_DRATE = ADS_readReg(ADSREG_DRATE);
	if (readback_DRATE != USER_DRATE) {
		while(1); //Stop, communication error with the ads1
	}
	ADS_CS1(1);
	ADS_CS2(0); //Now select ADS2
	readback_DRATE = ADS_readReg(ADSREG_DRATE);
	if (readback_DRATE != USER_DRATE) {
		while(1); //Stop, communication error with the ads2
	}
	ADS_CS2(1);
}

extern void ADS_waitRDY(void)
{
	while (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9));	//DRDY1
	while (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8));	//DRDY2
}

extern void ADS_CS1(uint8_t state)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, (GPIO_PinState)state);
}

extern void ADS_CS2(uint8_t state)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, (GPIO_PinState)state);
}
