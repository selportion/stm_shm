#include <_ansi.h>
#include <_syslist.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/times.h>
#include <limits.h>
#include <signal.h>
#include <../Inc/retarget.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

UART_HandleTypeDef *gHuart;

volatile int txComplete=1;
static uint8_t txBuffer[200];

void HAL_UART_TxCpltCallback (UART_HandleTypeDef *huart)
{
  txComplete=1;
}

int _write(int file, char *ptr, int len)
{
  // wait until the buffer is free
  while (!txComplete);

  // mark the buffer as "in use"
  txComplete=0;
  // copy the string into the buffer (with size limit)
  if (len>sizeof(txBuffer)) {
    len=sizeof(txBuffer);
  }
  memcpy(txBuffer,ptr,len);
  // send the buffer
  HAL_StatusTypeDef hstatus;
  hstatus = HAL_UART_Transmit_IT(gHuart, txBuffer, len);
  if (hstatus == HAL_OK)
	return len;
  else
	return EIO;
}

void RetargetInit(UART_HandleTypeDef *huart) {
  gHuart = huart;
}

